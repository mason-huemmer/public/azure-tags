# -------------------------------------------------------------------------------
# BUILD STAGE
# -------------------------------------------------------------------------------
FROM docker.io/library/python:3.10-slim-buster as builder

# PYTHON PACKAGE DIRECTORY
ARG DIRECTORY=src/azure-tags

# INSTALL PDM
RUN pip install -U pip setuptools wheel
RUN pip install pdm

# COPY FILES
COPY ${DIRECTORY}/pyproject.toml ${DIRECTORY}/pdm.lock ${DIRECTORY}/README.md /project/
COPY ${DIRECTORY}/azure /project/azure
COPY ${DIRECTORY}/tags /project/tags

# INSTALL DEPENDENCIES AND PROJECT
WORKDIR /project
RUN pdm install --prod --no-lock --no-editable

# -------------------------------------------------------------------------------
# RUN STAGE
# - Install Azure-Cli, Helm, Kubectl, Yq, Jq, Prometheus Client
# -------------------------------------------------------------------------------
FROM docker.io/library/python:3.10-slim-buster

# RETRIEVE PACKAGES FROM BUILD STAGE
ENV PYTHONPATH=/project/pkgs
COPY --from=builder /project/__pypackages__/3.10/lib /project/pkgs

# INSTALL PREREQUISITES
RUN apt-get update --yes
RUN apt-get install --yes ca-certificates curl apt-transport-https lsb-release gnupg

# INSTALL AZURE-CLI - https://docs.microsoft.com/en-us/cli/azure/install-azure-cli-linux?pivots=apt
RUN curl -sL https://aka.ms/InstallAzureCLIDeb | bash

# INSTALL GIT
RUN apt-get install --yes git

CMD [ "/bin/sh", "-c", "bash" ]
