# --------------------------------------------------------------------------------------------
# Copyright (c) Mason Huemmer. All rights reserved.
# Licensed under the MIT License. See License.txt in the project root for license information.
# --------------------------------------------------------------------------------------------

import sys
import platform
import logging
import logging.config

from datetime import datetime

APP_LOGGER_NAME = 'tags'
logger_names = [APP_LOGGER_NAME]

LOG_FILE_ENCODING = 'utf-8'
LOG_FILE_DIR = "/tmp/azure_tags_{}.log".format(datetime.now().strftime('%Y_%m_%d_%H%M'))

LOG_LEVEL='INFO'


class _ExcludeErrorsFilter(logging.Filter):
    def filter(self, record):
        """Filters out log messages with log level ERROR (numeric value: 40) or higher."""
        return record.levelno < 40


class _RedactingFilter(logging.Filter):
    def __init__(self, patterns):
        self._patterns = patterns

    def filter(self, record):
        record.msg = self.redact(record.msg)
        if isinstance(record.args, dict):
            for k in record.args.keys():
                record.args[k] = self.redact(record.args[k])
        else:
            record.args = tuple(self.redact(arg) for arg in record.args)
        return True

    def redact(self, msg):
        msg = str(msg)
        for pattern in self._patterns:
            msg = msg.replace(pattern, "***")
        return msg

class _HostnameFilter(logging.Filter):
    def __init__(self):
        self.hostname = platform.node()

    def filter(self, record):
        record.hostname = self.hostname
        return True

class _CustomFormatter(logging.Formatter):
    """Logging colored formatter, adapted from https://stackoverflow.com/a/56944256/3638629"""

    def __init__(self, enable_color):
        self.font_colors = {
            "grey": "\x1b[1;90m",
            "yellow": "\x1b[33;20m",
            "red": "\x1b[38;5;196m",
            "bold_red": "\x1b[31;1m",
            "purple": "\x1b[1;35m",
            "blue": "\x1b[34m",
            "green": "\x1b[32m",
            "cyan": "\033[36m",
            "white": "\x1b[37m",
            "reset": "\x1b[0m",
        }
        self.enable_color = enable_color

    def format(self, record):

        if self.enable_color:
            # SET DEFAULTS
            hostname = (
                self.font_colors["white"]
                + "["
                + self.font_colors["purple"]
                + "%(hostname)s"
                + self.font_colors["white"]
                + "]"
            )
            datetime = self.font_colors["green"] + "%(asctime)s"
            lineno = (
                self.font_colors["white"]
                + "(line "
                + self.font_colors["blue"]
                + "%(lineno)s"
                + self.font_colors["white"]
                + ")"
            )

            # SET LEVELNAME
            levelname = ""
            if record.levelno == logging.DEBUG:
                levelname = self.font_colors["green"] + "%(levelname)s"
            elif record.levelno == logging.INFO:
                levelname = self.font_colors["white"] + "%(levelname)s"
            elif record.levelno == logging.WARNING:
                levelname = self.font_colors["yellow"] + "%(levelname)s"
            elif record.levelno == logging.ERROR:
                levelname = self.font_colors["red"] + "%(levelname)s"
            else:
                levelname = self.font_colors["white"] + "%(levelname)s"

            # SET FILENAME
            filename = ""
            if record.levelno == logging.DEBUG:
                filename = self.font_colors["blue"] + "%(name)s"
            elif record.levelno == logging.INFO:
                filename = self.font_colors["white"] + "%(name)s"
            elif record.levelno == logging.WARNING:
                filename = self.font_colors["yellow"] + "%(name)s"
            elif record.levelno == logging.ERROR:
                filename = self.font_colors["red"] + "%(name)s"
            else:
                filename = self.font_colors["white"] + "%(name)s"

            # SET MESSAGE
            message = ""
            if record.levelno == logging.DEBUG:
                message = self.font_colors["green"] + "%(message)s"
            elif record.levelno == logging.INFO:
                message = self.font_colors["white"] + "%(message)s"
            elif record.levelno == logging.WARNING:
                message = self.font_colors["yellow"] + "%(message)s"
            elif record.levelno == logging.ERROR:
                message = self.font_colors["red"] + "%(message)s"
            else:
                message = self.font_colors["white"] + "%(message)s"

            format = f'{hostname} {datetime} {filename} {lineno} | {levelname} {message} {self.font_colors["reset"]}'
        else:
            format = "[%(hostname)s] %(asctime)s %(name)s (line %(lineno)s) | %(levelname)s %(message)s"

        formatter = logging.Formatter(format)
        return formatter.format(record)

config = {
    "version": 1,
    "filters": {
        "exclude_errors": {"()": _ExcludeErrorsFilter},
        "include_hostname": {"()": _HostnameFilter},
    },
    "formatters": {
        # Modify log message format here or replace with your custom formatter class
        "main_formatter": {"()": _CustomFormatter, "enable_color": True},
    },
    "handlers": {
        "console_stderr": {
            # Sends log messages with log level ERROR or higher to stderr
            "class": "logging.StreamHandler",
            "level": "ERROR",
            "filters": ["include_hostname"],
            "formatter": "main_formatter",
            "stream": sys.stderr,
        },
        "console_stdout": {
            # Sends log messages based on console log level
            "class": "logging.StreamHandler",
            "level": LOG_LEVEL,
            "formatter": "main_formatter",
            "filters": ["exclude_errors", "include_hostname"],
            "stream": sys.stdout,
        },
        "log_file": {
            # Sends all log message to file
            "class": "logging.handlers.RotatingFileHandler",
            "level": "DEBUG",
            "formatter": "main_formatter",
            "filters": [ "include_hostname" ],
            "maxBytes": 10485760,
            "mode": "a",
            "filename": LOG_FILE_DIR,
            "encoding": LOG_FILE_ENCODING,
        }
    },
    "root": {
        # In general, this should be kept at 'NOTSET'.
        # Otherwise it would interfere with the log levels set for each handler.
        "level": "NOTSET",
        "handlers": ["console_stdout", "log_file"],
    },
}

logging.config.dictConfig(config)

def get_logger(module_name=None):
    """ Get the logger for a module. If no module name is given, the current logger is returned.

    Example:
        get_logger(__name__)

    :param module_name: The module to get the logger for
    :type module_name: str
    :return: The logger
    :rtype: logger
    """

    if module_name:
        logger_name = '{}'.format(module_name)
    else:
        logger_name = APP_LOGGER_NAME
    return logging.getLogger(logger_name)